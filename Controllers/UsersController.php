<?php

Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		parent::__construct();
		$this->usersRepository = new usersRepository();
	}
public function defaultAction(){
		$this->view->render(__METHOD__);
	}

public function newAction(){
		if (!array_key_exists("user", $_SESSION)){
			$this->view->render(__METHOD__);
		} else {
			$_SESSION["alerts"][] = "Can not register because user already logged in";
			header("Location: index.php");
		}
		
	}

	public function createAction(){
		if ($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			foreach ($_POST as $key => $value) {
				if(property_exists($user,Helper::prepareProperty($key,"users"))){
					$user->{"set".Helper::prepareProperty($key,"method")}($value);
				} 
			}
			
			$this->usersRepository->insert($user);
			
			$this->view->render(__METHOD__);
		} else {
			header("Location: login.php");
		}
	}

	public function loginFormAction(){
		$this->view->render(__METHOD__);
	}

		public function conectareAction(){
		$this->view->render(__METHOD__);
	}

	public function loginAction(){
		
		if ($_SERVER["REQUEST_METHOD"]=="POST"){

			$name = $_POST["name"];
			$email = $_POST["email"];
			$user = $this->usersRepository->checkCredentials($name,$email);
			//var_dump($user);
			if ($user!=NULL){
				$_SESSION["user"] = $user;
			} else {
				$_SESSION["alerts"][] = "Wrong Username and Password";
			}
			header("Location: index.php?C=Users&A=loginForm");
		} else {
				echo "serios 2";
			header("Location: index.php?C=Users&A=conectare");
		}	
		
	}

	public function logoutAction(){
		if (array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		header("Location: index.php");
	}
}

