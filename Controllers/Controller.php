<?php

Abstract Class Controller {

	public $view;

	public function __construct(){
		$this->view = new View();
	}

	abstract public function defaultAction();

}