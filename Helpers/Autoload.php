<?php

function __autoload($className){
	$paths = array(
		"Controllers",
		"Helpers",
		"Helpers/Traits",
		"Models",
		"Models/Repositories",
		"Views",
		"Views/Users",
		"Views/Default"
	);
	foreach ($paths as $path){
		$path = $path."/".$className.".php";
		if (file_exists($path)) {
			require_once $path;
		}
	}
	//var_dump($path);
}