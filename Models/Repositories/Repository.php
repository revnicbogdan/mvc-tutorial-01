<?php


Abstract Class Repository {

  protected $model;
  protected $table;
  protected $database;


  public function __construct(){
    $this->database = new Database();
    $this->model = preg_replace('/Repository$/','',get_called_class());
   // var_dump($this->model = preg_replace('/Repository$/','',get_called_class()));
    $this->table = strtolower(implode("_", preg_split('/(?=[A-Z])/', preg_replace('/Repository$/','', lcfirst(get_called_class())))));
  }


  public function findByUid($uid){
    $result = $this->database->executeQuery("SELECT * FROM $this->table WHERE uid=$uid");
    $result = mysqli_fetch_assoc($result);
    $object = new $this->model;
    foreach ($result as $key => $value){
      if (property_exists($object, Helper::prepareProperty($key,"property"))) {
        $object->{"set".Helper::prepareProperty($key,"method")}($value);
      }
    }
    return $object;
  }
   public function insert($object){
    $fields = array();
    $values = array();
    foreach($object->properties as $key){
     $temporary = $object->{"get".Helper::prepareProperty($key,"method")}();
     if ($key!="uid" && !empty($temporary)){ 
      array_push($fields,$key);
      array_push($values,'"'.$temporary.'"');
     }

    }
    $fields = implode(", ",$fields);
    $values = implode(", ",$values);
    return $this->database->executeQuery("INSERT INTO $this->table ($fields) VALUES ($values);"); 
   }
/*
  public function insert($object){
		$fields = array();
		$values = array();
		foreach ($object as $key => $value){
			if (!empty($value)){
				array_push($fields,$key);
				array_push($values,'"'.$value.'"');
				
			}
		}
		$fields = implode(", ",$fields);
		$values = implode(", ",$values);
		$this->database->executeQuery("INSERT INTO $this->table ($fields) VALUES ($values);"); 
		//var_dump("INSERT INTO $this->table ($fields) VALUES ($values);"); 
	}
*/

}


?>