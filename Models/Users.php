<?php

	Class Users extends Model {


	public $properties = array("uid","name","email","password");

	private $name;
	private $email;
	private $password;

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getEmail(){
		return $this->email;
	}
	public function setEmail($email){
		$this->email = $email;
	}

	public function getPassword(){

		return $this->password;
	}
	public function setPassword($password){
		$this->password=$password;
	}
}

?>