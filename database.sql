DROP DATABASE IF EXISTS shoptest;

CREATE DATABASE IF NOT EXISTS shoptest;

USE shoptest;

CREATE TABLE IF NOT EXISTS users ( 
	uid INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL, 
	pid INT(11) DEFAULT 0, 
	name VARCHAR(255), 
	email_address VARCHAR(255),
	password VARCHAR(255)
	
);

INSERT INTO users 
	(name,email_address,password)
	VALUES
	("Bogdan","revnic.bogdan@yahoo.ro","12345"),
	("Radu","radu.sefu@yahoo.com","123456");