	<?php
	
if (isset($_SESSION["user"])){
  $user = $_SESSION["user"];
  ?>
  <div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-sm-offset-1">
				<h2>Welcome
					<?php  echo $user->getName();?>
|					  	<a href="index.php?C=Users&A=edit">Edit Account</a>
|					 	<a href="index.php?C=Users&A=logout">Logout</a>
				</h2>
				</div>
			</div>
	</div>
<?php
} else {
?>


	<div class="container">
			<div class="row">
				
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="POST" action="index.php?C=Users&A=login">
							
							<input type="text" name="name" placeholder="Name" />
							<input type="email" name="email" placeholder="email_address" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						
						<form method="POST" action="index.php?C=Users&A=create">
							
							<input type="text" name="name" placeholder="Name"/>
							<input type="email" name="email" placeholder="Email Address"/>
							<input type="password" name="password" placeholder="Password"/>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
						
					</div><!--/sign up form-->
				</div>
			</div>
		</div>

<?php
}

?>